var map2 = function () {
    for (var idx = 0; idx < this.credit.length; idx++) {
        emit(this.credit[idx].currency, this.credit[idx].balance);
    }
};

var reduce2 = function (key, value) {
    return Array.sum(value);
};

db.people.mapReduce(
    map2,
    reduce2,
    {
        out: "sumBalance"
    }
)

printjson(db.sumBalance.find().toArray())