printjson(db.people.aggregate(
    [
        { $group: {_id: "$nationality"}, bmi: { $divide: [ "$weight", {$multiply: ["$height", 2]} ] }, sumBmi: {$sum: "bmi"}, orders_ids: { $addToSet: "$_id" }},
        { $project: {value: { minBMI: {$min: "$bmi"}, maxBMI: {$max: "bmi"}, avgBMI: { $divide: [ "$sumBmi", { $size: "$orders_ids" } ]}}}}
    ]
).toArray())