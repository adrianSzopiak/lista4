var map2 = function () {
    for (var idx = 0; idx < this.credit.length; idx++) {
        var key = this.credit[idx].currency;
        var value = { count: 1, balance: this.credit[idx].balance };
        emit(key, value);
    }
};

var reduce2 = function (key, value) {
    var reducedVal = { count: 0, balance: 0 };

    for (var idx = 0; idx < value.length; idx++) {
        reducedVal.count += value[idx].count;
        reducedVal.balance += value[idx].balance;
    }
    return reducedVal;
};

var finalize2 = function (key, reducedVal) {
    reducedVal.avgBalance = reducedVal.qty / reducedVal.count;
    return reducedVal;
};

db.people.mapReduce(
    map2,
    reduce2,
    {
        out: "avgBalance",
        query: {
            sex: {$eq: "Female"},
            nationality: {$eq: "Poland"}
        },
        finalize: finalize2
    }
)

printjson(db.avgBalance.find().toArray())
