var map2 = function () {
    var value = {
        bmi: this.weight / this.height * 2,
        min: 0,
        max: 0
    };
    emit(this.nationality, value);
};

var reduce2 = function (sex, values) {
    var reducedVal = {bmi: 0, min: values[0].bmi, max: values[0].bmi};
    reducedVal.bmi = Array.sum(values.map(x => x.bmi)) / values.length;
    values.forEach(function (val) {
        if (val.bmi < reducedVal.min) reducedVal.min = val.min;
        if (val.bmi > reducedVal.max) reducedVal.max = val.max;
    })
    return reducedVal;
};

db.people.mapReduce(
    map2,
    reduce2,
    {
        out: "bmi",
        finalize: finalize2
    }
)

printjson(db.bmi.find().toArray())