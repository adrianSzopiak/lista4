db.people.aggregate( [
    { $match: {
            sex: {$eq: "Female"},
            nationality: {$eq: "Poland"}
    }},
    { $unwind: "$credit"},
    { $group: { _id: "$credit.currency", sum: { $sum: "$credit.balance"}, orders_ids: { $addToSet: "$_id" }}},
    { $project: { value: { sum: "$sum", avg: { $divide: [ "$sum", { $size: "$orders_ids" } ] }}}},
    { $merge: { into: "agg_alternative_3", on: "_id", whenMatched: "replace",  whenNotMatched: "insert" } }
] )