var map = function () {
    emit({job: this.job}, {count: 1});
}

var reduce = function (key, values) {
    var count = 0;

    values.forEach(
        function (val) {
            count += val['count'];
        }
    );

    return {count: count};
};
db.people.mapReduce(
    map,
    reduce,
    {
        out: 'jobs',
        verbose: true
    }
);

printjson(db.jobs.find().toArray())