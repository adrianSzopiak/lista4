var map2 = function () {
    var value = {
        height: this.height,
        weight: this.weight
    };
    emit(this.sex, value);
};

var reduce2 = function (sex, values) {
    var reducedVal = {height: 0, weight: 0};
    reducedVal.height = Array.sum(values.map(x => x.height)) / values.length;
    reducedVal.weight = Array.sum(values.map(x => x.weight)) / values.length;
    return reducedVal;
};

db.people.mapReduce(
    map2,
    reduce2,
    {
        out: "avg"
    }
)

printjson(db.avg.find().toArray())